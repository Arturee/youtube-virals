package com.fingera.viral;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import static com.fingera.viral.TestsUtil.*;


public class ViralsTest {

	
	@Test
	public void getViralVideos_correctInput_correctOutput() throws IOException, UsageException {
		List<Video> result = Virals.getViralVideos("female", "1.1.2011", "20.2.2020", 2, new RestClientMock());
		List<Video> expected = Arrays.asList(
				video(
						"pa14VNsdSYM",
						"Rihanna - Only Girl (In The World)",
						"Get Rihanna's eighth ...",
						"https://i.ytimg.com/vi/pa14VNsdSYM/hqdefault.jpg",
						"2010-10-12T22:31:11.000Z",
						583954539, //view
						1394124, //like
						65595, //dis
						0,
						108251 //comment
					),
				video(
						"A",
						"Rihanna - Only Girl (In The World)",
						"Get Rihanna's eighth ...",
						"https://i.ytimg.com/vi/pa14VNsdSYM/hqdefault.jpg",
						"2010-10-12T22:31:11.000Z",
						1000000, //view
						1394124, //like
						65595, //dis
						0,
						108251 //comment
					)
				);
		assertVideolistsEqual(result, expected);
	}
	
	
	
	
}
