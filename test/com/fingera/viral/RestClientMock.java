package com.fingera.viral;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;



class RestClientMock implements IRestClient {
	
	
	public String getThesaurusSynonyms(String word) throws IOException {
		if (word.equals("girl")){
			return read("thesaurGirl.json");
		} else if (word.equals("female")){
			return read("thesaurFemale.json");
		} else {
			return read("thesaurEmpty.json");
		}
	}
	public String getYoutubeVideoSearchResults(String word, Params params) throws IOException {
		if (word.equals("girl")){
			return read("youtubesearchA.json");
		} else if (word.equals("female")) {
			return read("youtubesearchB.json");
		} else if (word.equals("malformed")) {
			return read("youtubeMalformedArtificial.json");
		} else {
			return read("youtubesearchEmpty.json");
		}	
	}
	public String getYoutubeVideoStatistics(String id) throws IOException {
		//if (Arrays.asList("A", "B", "C", "pa14VNsdSYM").contains(id)){
		return read("youtubestats_"+ id + ".json");	
	}
	
	
	
	
	
	/*privates*******************/
	private static String read(String name) throws IOException {
		return FileUtils.readFileToString(new File("testJsons/" + name), "utf-8");
	}
}
