package com.fingera.viral;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import static com.fingera.viral.TestsUtil.*;
import static org.junit.Assert.*;

public class VideoTest {

	@Test
	public void getVideos_correctInput_correcOutput()
			throws IOException, UsageException {
		universalTest();
	}
	@Test
	public void getVideos_correctInput_uniqueOutput()
			throws IOException, UsageException {
		universalTest();
	}
	@Test
	public void getVideos_correctInput_sortedOutput()
			throws IOException, UsageException {
		universalTest();
	}
	@Test
	public void getVideos_limitSet_limitedOutput()
			throws IOException, UsageException {
		universalTest();
	}	
	@Test
	public void getVideos_nosenseKeyword_NoResultsFound()
			throws IOException, UsageException {
		List<String> synonyms = Arrays.asList("::F:S:SFasfasfasdfawefwerwerwerwersdfafasfafasdfsdfsdferwerwerwerwerlalallalalalrjewijoijjfjodjiosf");
		Params params = Params.sanitize("noresult", "1.1.2011", "2.2.2012", 3);
		List<Video> result = Video.getVideos(synonyms, params, new RestClientMock());
		List<Video> expected = new ArrayList<>();
		assertVideolistsEqual(result, expected);
	}
	@Test
	public void getVideos_malformedJsonFromYoutube_returnsEmptyListAndLogsAndDoesntCrash()
			throws IOException, UsageException {
		List<String> synonyms = Arrays.asList("malformed"); //hack to get a malformed json
		Params params = Params.sanitize("malformed", "1.1.2011", "2.2.2012", 3);
		Video.getVideos(synonyms, params, new RestClientMock());
		Video.getVideos(synonyms, params, new RestClientMock());
		List<Video> result = Video.getVideos(synonyms, params, new RestClientMock());
		assertTrue(result.isEmpty());
	}
	
	
	
	
	
	
	
	
	private static void universalTest() throws IOException, UsageException {
		List<String> synonyms = Arrays.asList("girl", "female");
		Params params = Params.sanitize("girl", "1.1.2011", "2.2.2012", 3);
		List<Video> result = Video.getVideos(synonyms, params, new RestClientMock());
		List<Video> expected = Arrays.asList(
				video(
						"pa14VNsdSYM",
						"Rihanna - Only Girl (In The World)",
						"Get Rihanna's eighth ...",
						"https://i.ytimg.com/vi/pa14VNsdSYM/hqdefault.jpg",
						"2010-10-12T22:31:11.000Z",
						583954539, //view
						1394124, //like
						65595, //dis
						0,
						108251 //comment
					),
				video(
						"A",
						"Rihanna - Only Girl (In The World)",
						"Get Rihanna's eighth ...",
						"https://i.ytimg.com/vi/pa14VNsdSYM/hqdefault.jpg",
						"2010-10-12T22:31:11.000Z",
						1000000, //view
						1394124, //like
						65595, //dis
						0,
						108251 //comment
					),
				video(
						"C",
						"Rihanna - Only Girl (In The World)",
						"Get Rihanna's eighth ...",
						"https://i.ytimg.com/vi/pa14VNsdSYM/hqdefault.jpg",
						"2010-10-12T22:31:11.000Z",
						7000, //view
						1394124, //like
						65595, //dis
						0,
						108251 //comment
					)
				);
		assertVideolistsEqual(result, expected);
	}
}
