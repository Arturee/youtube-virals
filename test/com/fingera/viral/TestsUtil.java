package com.fingera.viral;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

class TestsUtil {

	public static Video video(String id, String title, String description, String thumbUrl, String datePublished,
			int viewCount, int likeCount, int dislikeCount, int favoriteCount, int commentCount){
		DateFormat youtubeDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
		Video video = new Video();
		video.id = id;
		video.title = title;
		video.description = description;
		video.thumbUrl = thumbUrl;	
		video.viewCount = viewCount;
		
		//video.likeCount = likeCount;
		//video.dislikeCount = dislikeCount;
		//video.favoriteCount = favoriteCount;
		//video.commentCount = commentCount;
		try {
			video.datePublished = youtubeDateFormat.parse(datePublished);	
		} catch(ParseException e){
			throw new RuntimeException("bad date manually in test");
		}
		return video;
	}
	public static boolean videosEqual(Video a, Video b){
		return (
				a.id.equals(b.id) &&
				a.title.equals(b.title) &&
				a.description.equals(b.description) &&
				a.datePublished.equals(b.datePublished) &&
				a.thumbUrl.equals(b.thumbUrl) &&
				a.viewCount == b.viewCount
				
				//a.likeCount == b.likeCount &&
				//a.dislikeCount == b.dislikeCount &&
				//a.favoriteCount == b.favoriteCount &&
				//a.commentCount == b.commentCount
			);
	}
	public static void assertVideolistsEqual(List<Video> a, List<Video> b){
		assertEquals(a.size(), b.size());
		for (int i=0; i<a.size(); i++){
			assertTrue("Video1 id=" + a.get(i).id + ", video2 id = " + b.get(i).id, videosEqual(a.get(i), b.get(i)));
		}
	}

}
