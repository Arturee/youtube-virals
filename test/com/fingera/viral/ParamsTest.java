package com.fingera.viral;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ParamsTest {
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	
	
	@Test
	public void sanitize_correctInput_correctOutput() throws UsageException {
		Params params = Params.sanitize("girl", "1.1.2011", "2.2.2012", 6);	
		assertEquals(params.limit, 6);
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		assertEquals(format.format(params.publishedAfter), "01.01.2011");
		assertEquals(format.format(params.publishedBefore), "02.02.2012");
	}
	@Test
	public void sanitize_missingInput_DefaultsUsed() throws UsageException {
		Params params = Params.sanitize("girl", null, null, null);
		assertEquals(params.limit, 5);
		assertEquals(params.publishedAfter, null);
		assertEquals(params.publishedBefore, null);
	}
	@Test
	public void sanitize_publishedAfterWrongFormat_throwsUsageExceptionWithCorrectText() throws UsageException {
		thrown.expect(UsageException.class);
		thrown.expectMessage("Impossible date or wrong date format: \"2-1-2011\" (use eg. 12.24.2012)");
		
		Params.sanitize("girl", "2-1-2011", null, 10);
	}
	@Test
	public void sanitize_publishedBeforeWrongFormat_throwsUsageExceptionWithCorrectText() throws UsageException {
		thrown.expect(UsageException.class);
		thrown.expectMessage("Impossible date or wrong date format: \"mueheheh not a date actually\" (use eg. 12.24.2012)");
		
		Params.sanitize("girl", null, "mueheheh not a date actually", 10);
	}
	@Test
	public void sanitize_impossibleDate_throwsUsageExceptionWithCorrectText() throws UsageException {
		thrown.expect(UsageException.class);
		thrown.expectMessage("Impossible date or wrong date format: \"31.2.2011\" (use eg. 12.24.2012)");
		
		Params.sanitize("girl", null, "31.2.2011", 10);
	}
	@Test
	public void sanitize_invertedDates_throwsUsageExceptionWithCorrectText() throws UsageException{
		thrown.expect(UsageException.class);
		thrown.expectMessage("conflicting times/dates (published after must be sooner than publishedBefore... obviously)");
		
		Params.sanitize("girl", "2.1.2011", "1.1.2011", 10);
	}
	@Test
	public void sanitize_sameDates_throwsUsageExceptionWithCorrectText() throws UsageException{
		thrown.expect(UsageException.class);
		thrown.expectMessage("conflicting times/dates (published after must be sooner than publishedBefore... obviously)");
		
		Params.sanitize("girl", "1.1.2011", "1.1.2011", 10);
	}
	@Test
	public void sanitize_dateFromTheFuture_throwsUsageExceptionWithCorrectText() throws UsageException{
		thrown.expect(UsageException.class);
		thrown.expectMessage("cannot search the future: published after 1.1.2022");
		
		Params.sanitize("girl", "1.1.2022", null, 10);
	}
	@Test
	public void sanitize_limitTooHigh_throwsUsageExceptionWithCorrectText() throws UsageException {
		thrown.expect(UsageException.class);
		thrown.expectMessage("wrong limit (must be between 1 and 50, both inclusive)");
		
		Params.sanitize("girl", "1.1.2011", "2.2.2012", 100);
	}
	@Test
	public void sanitize_limitZero_throwsUsageExceptionWithCorrectText() throws UsageException {
		thrown.expect(UsageException.class);
		thrown.expectMessage("wrong limit (must be between 1 and 50, both inclusive)");
		
		Params.sanitize("girl", "1.1.2011", "2.2.2012", 0);
	}
	@Test
	public void sanitize_limitNegative_throwsUsageExceptionWithCorrectText() throws UsageException {
		thrown.expect(UsageException.class);
		thrown.expectMessage("wrong limit (must be between 1 and 50, both inclusive)");
		
		Params.sanitize("girl", "1.1.2011", "2.2.2012", -1);
	}
	@Test
	public void sanitize_wordNull__throwsUsageExceptionWithCorrectText() throws UsageException {
		thrown.expect(UsageException.class);
		thrown.expectMessage("no keyword");
		
		Params.sanitize(null, "1.1.2011", "2.2.2012", 10);
	}
	@Test
	public void sanitize_wordEmpty_throwsUsageExceptionWithCorrectText() throws UsageException {
		thrown.expect(UsageException.class);
		thrown.expectMessage("no keyword");
		
		Params.sanitize("", "1.1.2011", "2.2.2012", 10);
	}
}
