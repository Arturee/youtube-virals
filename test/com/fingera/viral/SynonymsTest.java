package com.fingera.viral;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class SynonymsTest {


	@Test
	public void get_correctInput_correctOutput()
			throws IOException {
		universalTest();
	}
	@Test
	public void get_correctInput_uniqueOutput()
			throws IOException {
		universalTest();
	}
	@Test
	public void get_correctInput_outputContainsOriginalWord()
			throws IOException {
		universalTest();
	}
	@Test
	public void get_correctInput_noAntonymsInOutput()
			throws IOException {
		universalTest();
	}	
	@Test
	public void get_limitSet_limitedOutput()
			throws IOException {
		List<String> result = Synonyms.get( "girl", 3, new RestClientMock());
		assertEquals(result.size(), 3);
	}
	@Test
	public void get_unknownWord_returnsOriginalWord()
			throws IOException {
		List<String> result = Synonyms.get("yellow dog has bannanas for dinner", 3, new RestClientMock());
		assertEquals(result, Arrays.asList("yellow dog has bannanas for dinner"));
	}

	
	
	
	
	
	
	
	
	private static void universalTest() throws IOException {
		List<String> result = Synonyms.get( "girl", 1000, new RestClientMock());
		List<String> expected = Arrays.asList("girl", "miss", "missy", "young lady", "young woman", "female", "female person");
		result.sort((a,b)->{return a.compareTo(b);});
		expected.sort((a,b)->{return a.compareTo(b);});
		assertEquals(result, expected);
	}
}
