*This project is public only for presentation purposes. You have no permission to use, modify, or share the software.*

### Technologies ###

Java, Apache CFX 2.0

### Introduction ####

Youtube-virals is a SOAP web service, which delivers a list of most viewed videos based on an input word (sorted from most viewed to least).
Videos can be filtered by published date. Length of the list is limited to 50 max. This service is dependent on two other services -
Youtube and Thesaurus. Videos are searched by the input word and up to 20 of it's synonyms separately and the most viewed videos out of
the combined results are returned. Everything is well tested.

![architecture](readme/viral.jpg)

### Example ###

(see examples/ for more)


```
#!url
curl -i --header "Content-Type:text/xml;charset=UTF-8" --data @soapRequest.xml http://localhost:9090/ViralsPort

```

Request  
```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
 <env:Header></env:Header>
 <env:Body>
	<v:getVirals xmlns:v="http://viral.fingera.com/">
		<keyword>girl</keyword>
		<publishedAfter>1.4.2017</publishedAfter>
		<publishedBefore>21.4.2017</publishedBefore>
		<limit>4</limit>
	</v:getVirals>
 </env:Body>
</env:Envelope>
```

* any argument except keyword can be missing (the whole element must be missing. Element with no value will cause an error)
* limit between 1 and 50 (default 5)


Response  
```
#!xml
<?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ns2:getViralsResponse xmlns:ns2="http://viral.fingera.com/">
      <video>
        <id>WtJ1G4li_b8</id>
        <title>Wonder Woman &amp; Baby eat too much candy! w/ Pink Spidergirl Doctor, SpiderElsa, Baby Thief</title>
        <description>Wonder Woman &amp; Baby eat too much candy! w/ Pink Spidergirl Doctor, SpiderElsa, Baby Thief funny superhero video in real life by Superhero Fun factory!</description>
        <thumbUrl>https://i.ytimg.com/vi/WtJ1G4li_b8/hqdefault.jpg</thumbUrl>
        <datePublished>2017-04-09T08:00:01+02:00</datePublished>
        <viewCount>3877289</viewCount>
      </video>
      <video>
        <id>lvi2LbXcn6g</id>
        <title>Cheated by my girlfriend? (100% not clickbait) _ Catch A Lover _</title>
        <description>&#xE3;&#x192;&#x2026;&#xE3;&#x192;&#x2026; CHECK OUT MY GIVEAWAY OF MY THE SAME OCMPUTER I HAVE http://bit.ly/2ovdQyb Game: Catch A Lover ...</description>
        <thumbUrl>https://i.ytimg.com/vi/lvi2LbXcn6g/hqdefault.jpg</thumbUrl>
        <datePublished>2017-04-13T16:28:17+02:00</datePublished>
        <viewCount>3664136</viewCount>
      </video>
      <video>
        <id>5W-2g6QeYFI</id>
        <title>Women Try $1 Pregnancy Tests</title>
        <description>"...Pop Tarts are more expensive than this pregnancy test." Boldly BuzzFeedYellow has changed its name to Boldly. It's the same content you know and love just ...</description>
        <thumbUrl>https://i.ytimg.com/vi/5W-2g6QeYFI/hqdefault.jpg</thumbUrl>
        <datePublished>2017-04-17T22:49:34+02:00</datePublished>
        <viewCount>2682326</viewCount>
      </video>
      <video>
        <id>ZL1HCv0jh-w</id>
        <title>Women Model With Their Worst Fear</title>
        <description>I should've said I was scared of kittens." Boldly BuzzFeedYellow has changed its name to Boldly. It's the same content you know and love just Bolder. Subscribe ...</description>
        <thumbUrl>https://i.ytimg.com/vi/ZL1HCv0jh-w/hqdefault.jpg</thumbUrl>
        <datePublished>2017-04-08T15:00:33+02:00</datePublished>
        <viewCount>2524079</viewCount>
      </video>
    </ns2:getViralsResponse>
  </soap:Body>
</soap:Envelope>


```

* SOAP client must make sure to decode HTML UTF-8 numeric entities in response to normal UTF-8 chars

### Deployment ###

In order for everything to work correctly, you have to download and use the libraries listed below. You also have to
supply API keys for Youtube API and the Thesaurus in RestClient.java.


### Libs ###

* apache commons-io - for FileUtils (for testing)
* codehouse jettison - to process JSON
* apache CXF 2.0 - to generate SOAP web service (includes javax for ws annotations)