package com.fingera.viral;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


public class Video {
	public String id, title, description, thumbUrl; // hq thumb
	public Date datePublished;
	public int viewCount = -1;
	
	//likeCount, dislikeCount;
	//favoriteCount, commentCount;
	
	private static final Logger LOGGER = Logger.getLogger(Video.class.getName());
	static {
		try {
			FileHandler handler = new FileHandler("log.txt");
			handler.setFormatter(new SimpleFormatter());
			LOGGER.addHandler(handler);
		} catch(IOException | SecurityException e){
			throw new RuntimeException("LOGGER constructor problem" , e);
		}
	}
	
	
	
	
	
	
	/**
	 * @param synonyms
	 * @param params
	 * @param restClient
	 * @return can be empty
	 * @throws YoutubeFatalError
	 *             when youtube returns something unexpected due to an error in
	 *             youtube DB or service
	 * @throws IOException
	 */
	public static List<Video> getVideos(List<String> synonyms, Params params, IRestClient restClient)
			throws IOException {
				
		Set<Video> all = new HashSet<>();
		for (int i = 0; i < synonyms.size(); i++) {
			String synonym = synonyms.get(i);
			List<Video> videos = Video.get(synonym, params, restClient);
			all.addAll(videos);
		}
		List<Video> allList = Arrays.asList(all.toArray(new Video[0]));
		allList.sort((u, v) -> {
			return v.viewCount - u.viewCount;
		});
		if (params.limit < allList.size()) {
			return allList.subList(0, params.limit);
		} else {
			return allList;
		}
	}
	public Video() {
		/* hack: for JAXB */
	}
	@Override
	public boolean equals(Object o){
		return (o instanceof Video) && ((Video)o).id.equals(this.id);
	}
	@Override
	public int hashCode(){
		return this.id.hashCode();
	}
	

	
	
	
	
	
	
	
	// privates *************************************************************
	private static List<Video> get(String word, Params params, IRestClient restClient)
			throws IOException {
		String searchResultVideosJson = restClient.getYoutubeVideoSearchResults(word, params);
		List<Video> videos = Video.fromJson(searchResultVideosJson);
		for (Video video: videos){
			video.getStatistics(restClient);
		}
		return videos.stream().filter(v->v.viewCount >= 0).collect(Collectors.toList());
		//filters out videos where youtube didnt supply viewCount
	}

	private static List<Video> fromJson(String json){
		try {
			String errorInYoutubeResponse = null;
			List<Video> videos = new ArrayList<>();
			JSONArray items = new JSONObject(json).getJSONArray("items");
			for (int i = 0; i < items.length(); i++) {
				JSONObject item = items.getJSONObject(i);
				JSONObject snippet = item.getJSONObject("snippet");
				String datetext = snippet.getString("publishedAt");
				try {
					DateFormat youtubeDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
					// eg. "2010-10-12T22:31:11.000Z"
					videos.add(new Video(item.getJSONObject("id").getString("videoId"), snippet.getString("title"),
							snippet.getString("description"),
							snippet.getJSONObject("thumbnails").getJSONObject("high").getString("url"),
							youtubeDateFormat.parse(datetext)));
				} catch (ParseException e) {
					errorInYoutubeResponse = "date could not be parsed:";
				} catch (JSONException e) {
					errorInYoutubeResponse = "not all attributes are present:";
				}
			}
			if (errorInYoutubeResponse != null){
				LOGGER.log(Level.SEVERE, "{0}\n{1}", new String[]{errorInYoutubeResponse, json});
			}			
			return videos;
		} catch (JSONException e) {
			LOGGER.log(Level.SEVERE, "youtube search response doesn't contain 'items': {0}", json);
			return new ArrayList<Video>();
		}
	}

	private void getStatistics(IRestClient restClient) throws IOException {
		String json = restClient.getYoutubeVideoStatistics(this.id);
		try {
			JSONObject stats = new JSONObject(json).getJSONArray("items").getJSONObject(0).getJSONObject("statistics");
			this.viewCount = Integer.parseInt(stats.getString("viewCount"));
			
			//this.likeCount = Integer.parseInt(stats.getString("likeCount"));
			//this.dislikeCount = Integer.parseInt(stats.getString("dislikeCount"));
			//this.favoriteCount = Integer.parseInt(stats.getString("favoriteCount"));
			//this.commentCount = Integer.parseInt(stats.getString("commentCount"));
		} catch (JSONException|NumberFormatException e) {
			LOGGER.log(Level.SEVERE, "viewCount not present or erroneous:\n {0}", json);
		}
	}

	private Video(String id, String title, String description, String thumbUrl, Date datePublished) {
		this.id = id; // eg. "PIb6AZdTr-A"
		this.title = title;
		this.description = description;
		this.thumbUrl = thumbUrl;
		this.datePublished = datePublished;
	}

}
