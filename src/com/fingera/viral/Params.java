package com.fingera.viral;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class Params {
	public int limit;
	public Date publishedBefore, publishedAfter;
	
	public static Params sanitize(String keyword, String publishedAfter, String publishedBefore, Integer limit)
			throws UsageException {
		Params params = new Params(null, null, 5);
		if (limit != null){
			if (limit >= 1 && limit <= 50){
				params.limit = limit;
			} else {
				throw new UsageException("wrong limit (must be between 1 and 50, both inclusive)", null);
			}		
		}
		if (keyword == null || keyword.isEmpty()){
			throw new UsageException("no keyword", null);
		}
		params.publishedAfter = parseDate(params, publishedAfter);
		params.publishedBefore = parseDate(params, publishedBefore);
		Date now = new Date();
		if (params.publishedAfter != null){
			if (!params.publishedAfter.before(now)){
				throw new UsageException("cannot search the future: published after " + publishedAfter, null);
			}
			if (params.publishedBefore != null){
				if (!params.publishedAfter.before(params.publishedBefore)){
					throw new UsageException("conflicting times/dates (published after must be sooner than publishedBefore... obviously)", null);
				}
			}	
		}		
		return params;
	}
	
	
	
	
	//privates *************************
	private static Date parseDate(Params params, String date) throws UsageException {
		try {
			SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
			dateFormatter.setLenient(false);
			//makes sure parser doesn't tolerate impossible dates - eg. 41.12.2016
			if (date != null){
				return dateFormatter.parse(date);
			} else {
				return null;
			}
		} catch(ParseException e){
			throw new UsageException("Impossible date or wrong date format: \"" + date + "\" (use eg. 12.24.2012)", null);
		} 
	}
	
	private Params(Date publishedBefore, Date publishedAfter, int limit){
		this.limit=limit;
		this.publishedAfter = publishedAfter;
		this.publishedBefore = publishedBefore;
	}
}
