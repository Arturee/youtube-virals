package com.fingera.viral;

import javax.xml.ws.WebFault;

@WebFault(name = "BadUsage")
public class UsageException extends Exception {
	public UsageException(String message, Throwable cause) {
        super(message, cause);
    }
}
