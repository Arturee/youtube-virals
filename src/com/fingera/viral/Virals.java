package com.fingera.viral;

import java.io.IOException;
import java.util.List;

import javax.jws.WebService;


/**
 * This is the main class from which the web service is generated.
 * @author Artur
 *
 */
@WebService(
		targetNamespace = "http://viral.fingera.com/",
		endpointInterface = "com.fingera.viral.IVirals",
		portName = "ViralsPort",
		serviceName = "ViralsService"
		)
public class Virals implements IVirals {
	
	@Override
	public List<Video> getVirals(String keyword, String publishedAfter, String publishedBefore, Integer limit)
			throws NetworkException, UsageException {
		try {
			return Virals.getViralVideos(keyword, publishedAfter, publishedBefore, limit, new RestClient());
		} catch(IOException e){
			throw new NetworkException(e);
		}
	}
	
	
	
	
	
	//package **************************************
	static  List<Video> getViralVideos(String keyword, String publishedAfter, String publishedBefore,
			Integer limit, IRestClient restClient)
			throws IOException, UsageException {
		final int MAX_SYNONYMS = 5;
		Params params = Params.sanitize(keyword, publishedAfter, publishedBefore, limit);
		List<String> synonyms = Synonyms.get(keyword, MAX_SYNONYMS, restClient);
		return Video.getVideos(synonyms, params, restClient);
	}
}
