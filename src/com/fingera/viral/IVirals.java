package com.fingera.viral;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.ResponseWrapper;

@WebService(
		name = "IVirals",
		targetNamespace = "http://viral.fingera.com/"
		)
public interface IVirals {
	
	@WebMethod
	@WebResult(name="video") //wraps each element of the list
	@ResponseWrapper(localName="virals")
	public List<Video> getVirals(
			@WebParam(name = "keyword") String keyword,
			@WebParam(name = "publishedAfter") String publishedAfter,
			@WebParam(name = "publishedBefore") String publishedBefore,
			@WebParam(name = "limit") Integer limit
		)
			throws NetworkException, UsageException;
}