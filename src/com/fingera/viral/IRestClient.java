package com.fingera.viral;

import java.io.IOException;

interface IRestClient {

	public String getThesaurusSynonyms(String word) throws IOException;
	public String getYoutubeVideoSearchResults(String word, Params params) throws IOException;
	public String getYoutubeVideoStatistics(String id) throws IOException;
}
