package com.fingera.viral;

import javax.xml.ws.WebFault;

@WebFault(name = "NetworkProblem")
public class NetworkException extends Exception {
	public NetworkException(Throwable cause) {
        super("Please try again after a few minutes.", cause);
        
        //TODO log stack trace
    }
}
