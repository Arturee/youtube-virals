package com.fingera.viral;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.io.CachedOutputStream;

class RestClient implements IRestClient {
	@Override
	public String getThesaurusSynonyms(String word) throws IOException {
		AttrBuilder attrs = new AttrBuilder()
				.add("key", "USE YOUR OWN THERAUR KEY BRO")
				.add("language", "en_US")
				.add("output", "json")
				.add("word", word);
		final String THESAUR_URL = "http://thesaurus.altervista.org/thesaurus/v1";
		return getBody(THESAUR_URL + attrs.build());
	}
	@Override
	public String getYoutubeVideoSearchResults(String word, Params params) throws IOException {
		AttrBuilder attrs = new AttrBuilder()
				.add("key", "USE YOU OWN YOUTUBE API KEY BRO")
				.add("order", "viewCount")
				.add("part", "snippet")
				.add("type", "video")
				.add("maxResults", Integer.toString(params.limit))
				.add("q", word);
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
		if (params.publishedAfter != null){
			attrs.add("publishedAfter", dateFormatter.format(params.publishedAfter));
		}
		if (params.publishedBefore != null){
			attrs.add("publishedBefore", dateFormatter.format(params.publishedBefore));
		}
		final String YOUTUBE_SEARCH_URL = "https://www.googleapis.com/youtube/v3/search";
		return getBody(YOUTUBE_SEARCH_URL + attrs.build());
	}
	@Override
	public String getYoutubeVideoStatistics(String id) throws IOException {
		AttrBuilder attrs = new AttrBuilder()
				.add("key", "AIzaSyAIP24Jz-wG7jhdRhgPwyTamJPedY0SIYQ")
				.add("part", "statistics")
				.add("id", id);
		final String YOUTUBE_STATS_URL = "https://www.googleapis.com/youtube/v3/videos";
		return getBody(YOUTUBE_STATS_URL + attrs.build());
	}

	
	
	
	
	
	
	
	
	//privates ******************************
	private static String getBody(String url) throws IOException {
		try {
			InputStream input = new URL(url).openStream();
	        CachedOutputStream output  = new CachedOutputStream();
	        IOUtils.copy(input, output);
	        input.close();
	        output.close();
	        return output.getOut().toString();
		} catch(MalformedURLException e){
			throw new RuntimeException("URL malformed", e);
		}
    }
}
