package com.fingera.viral;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class AttrBuilder {
	private StringBuilder attrs = new StringBuilder();
	
	
	public AttrBuilder(){
		attrs.append("?");
	}
	public AttrBuilder add(String attr, String value){
		attrs.append("&").append(attr).append("=").append(urlencode(value));
		return this;
	}
	public String build(){
		return attrs.toString();
	}
	
	
	//privates *********
	private static String urlencode(String s){
		try {
			return URLEncoder.encode(s, java.nio.charset.StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e){
			throw new RuntimeException("UTF-8 not supported", e);
		}
	}
}
