package com.fingera.viral;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Synonyms {

	public static List<String> get(String word, int limit, IRestClient restClient)
			throws IOException {
		try {
			String json = restClient.getThesaurusSynonyms(word);
			JSONArray responses = new JSONObject(json).getJSONArray("response");
			Set<String> synonyms = new HashSet<>();
	        for (int i = 0; i < responses.length(); i++){
	        	String[] synonymsPart = responses.getJSONObject(i).getJSONObject("list").getString("synonyms").split("\\|"); 
	        	synonyms.addAll(Arrays.asList(synonymsPart));
	        }
	        List<String> synonymsList = synonyms.stream()
	        		.filter(synonym->!Pattern.matches(".*\\(antonym\\).*", synonym))
	        		.filter(synonym->!synonym.equals(word))
	        		.collect(Collectors.toList());
	        
	        synonymsList.add(0, word);
	        if (limit < synonymsList.size()){
	        	return synonymsList.subList(0, limit);
	        } else {
	        	return synonymsList;
	        }
		} catch(JSONException e){
			//no synonyms
			return Arrays.asList(word);
		}
	}
}
